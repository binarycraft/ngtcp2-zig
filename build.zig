const std = @import("std");

pub fn build(b: *std.build.Builder) !void {
    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const target = b.standardTargetOptions(.{});
    const mode = b.standardReleaseOptions();

    var config_cmd = std.ArrayList([]const u8).init(b.allocator);

    if (target.toTarget().cpu.arch == .x86_64) {
        if (target.isLinux()) {
            try config_cmd.appendSlice(&.{
                "./config",
                "linux-x86_64",
            });
        }

        if (target.isDarwin()) {
            try config_cmd.appendSlice(&.{
                "./config",
                "darwin64-x86_64-cc",
            });
        }

        if (target.isWindows()) {
            try config_cmd.appendSlice(&.{
                "./config",
                "mingw64",
            });
        }
    }

    if (target.toTarget().cpu.arch.isAARCH64()) {
        if (target.isLinux()) {
            try config_cmd.appendSlice(&.{
                "./config",
                "linux-aarch64",
            });
        }

        if (target.isDarwin()) {
            try config_cmd.appendSlice(&.{
                "./config",
                "darwin64-arm64-cc",
            });
        }

        if (target.isWindows()) {
            //@compileError("windows-aarch64 not supported");
        }
    }

    const config_gen = b.addSystemCommand(config_cmd.items);
    var crypto_src = std.ArrayList([]const u8).init(b.allocator);
    try crypto_src.appendSlice(&.{
        "deps/openssl/crypto/aes/aes_cbc.c",
        "deps/openssl/crypto/aes/aes_cfb.c",
        "deps/openssl/crypto/aes/aes_core.c",
        "deps/openssl/crypto/aes/aes_ecb.c",
        "deps/openssl/crypto/aes/aes_ige.c",
        "deps/openssl/crypto/aes/aes_misc.c",
        "deps/openssl/crypto/aes/aes_ofb.c",
        "deps/openssl/crypto/aes/aes_wrap.c",
        "deps/openssl/crypto/aria/aria.c",
        "deps/openssl/crypto/asn1/a_bitstr.c",
        "deps/openssl/crypto/asn1/a_d2i_fp.c",
        "deps/openssl/crypto/asn1/a_digest.c",
        "deps/openssl/crypto/asn1/a_dup.c",
        "deps/openssl/crypto/asn1/a_gentm.c",
        "deps/openssl/crypto/asn1/a_i2d_fp.c",
        "deps/openssl/crypto/asn1/a_int.c",
        "deps/openssl/crypto/asn1/a_mbstr.c",
        "deps/openssl/crypto/asn1/a_object.c",
        "deps/openssl/crypto/asn1/a_octet.c",
        "deps/openssl/crypto/asn1/a_print.c",
        "deps/openssl/crypto/asn1/a_sign.c",
        "deps/openssl/crypto/asn1/a_strex.c",
        "deps/openssl/crypto/asn1/a_strnid.c",
        "deps/openssl/crypto/asn1/a_time.c",
        "deps/openssl/crypto/asn1/a_type.c",
        "deps/openssl/crypto/asn1/a_utctm.c",
        "deps/openssl/crypto/asn1/a_utf8.c",
        "deps/openssl/crypto/asn1/a_verify.c",
        "deps/openssl/crypto/asn1/ameth_lib.c",
        "deps/openssl/crypto/asn1/asn1_err.c",
        "deps/openssl/crypto/asn1/asn1_gen.c",
        "deps/openssl/crypto/asn1/asn1_item_list.c",
        "deps/openssl/crypto/asn1/asn1_lib.c",
        "deps/openssl/crypto/asn1/asn1_par.c",
        "deps/openssl/crypto/asn1/asn_mime.c",
        "deps/openssl/crypto/asn1/asn_moid.c",
        "deps/openssl/crypto/asn1/asn_mstbl.c",
        "deps/openssl/crypto/asn1/asn_pack.c",
        "deps/openssl/crypto/asn1/bio_asn1.c",
        "deps/openssl/crypto/asn1/bio_ndef.c",
        "deps/openssl/crypto/asn1/d2i_pr.c",
        "deps/openssl/crypto/asn1/d2i_pu.c",
        "deps/openssl/crypto/asn1/evp_asn1.c",
        "deps/openssl/crypto/asn1/f_int.c",
        "deps/openssl/crypto/asn1/f_string.c",
        "deps/openssl/crypto/asn1/i2d_pr.c",
        "deps/openssl/crypto/asn1/i2d_pu.c",
        "deps/openssl/crypto/asn1/n_pkey.c",
        "deps/openssl/crypto/asn1/nsseq.c",
        "deps/openssl/crypto/asn1/p5_pbe.c",
        "deps/openssl/crypto/asn1/p5_pbev2.c",
        "deps/openssl/crypto/asn1/p5_scrypt.c",
        "deps/openssl/crypto/asn1/p8_pkey.c",
        "deps/openssl/crypto/asn1/t_bitst.c",
        "deps/openssl/crypto/asn1/t_pkey.c",
        "deps/openssl/crypto/asn1/t_spki.c",
        "deps/openssl/crypto/asn1/tasn_dec.c",
        "deps/openssl/crypto/asn1/tasn_enc.c",
        "deps/openssl/crypto/asn1/tasn_fre.c",
        "deps/openssl/crypto/asn1/tasn_new.c",
        "deps/openssl/crypto/asn1/tasn_prn.c",
        "deps/openssl/crypto/asn1/tasn_scn.c",
        "deps/openssl/crypto/asn1/tasn_typ.c",
        "deps/openssl/crypto/asn1/tasn_utl.c",
        "deps/openssl/crypto/asn1/x_algor.c",
        "deps/openssl/crypto/asn1/x_bignum.c",
        "deps/openssl/crypto/asn1/x_info.c",
        "deps/openssl/crypto/asn1/x_int64.c",
        "deps/openssl/crypto/asn1/x_long.c",
        "deps/openssl/crypto/asn1/x_pkey.c",
        "deps/openssl/crypto/asn1/x_sig.c",
        "deps/openssl/crypto/asn1/x_spki.c",
        "deps/openssl/crypto/asn1/x_val.c",
        "deps/openssl/crypto/async/arch/async_null.c",
        "deps/openssl/crypto/async/arch/async_posix.c",
        "deps/openssl/crypto/async/arch/async_win.c",
        "deps/openssl/crypto/async/async.c",
        "deps/openssl/crypto/async/async_err.c",
        "deps/openssl/crypto/async/async_wait.c",
        "deps/openssl/crypto/bf/bf_cfb64.c",
        "deps/openssl/crypto/bf/bf_ecb.c",
        "deps/openssl/crypto/bf/bf_enc.c",
        "deps/openssl/crypto/bf/bf_ofb64.c",
        "deps/openssl/crypto/bf/bf_skey.c",
        "deps/openssl/crypto/bio/b_addr.c",
        "deps/openssl/crypto/bio/b_dump.c",
        "deps/openssl/crypto/bio/b_print.c",
        "deps/openssl/crypto/bio/b_sock.c",
        "deps/openssl/crypto/bio/b_sock2.c",
        "deps/openssl/crypto/bio/bf_buff.c",
        "deps/openssl/crypto/bio/bf_lbuf.c",
        "deps/openssl/crypto/bio/bf_nbio.c",
        "deps/openssl/crypto/bio/bf_null.c",
        "deps/openssl/crypto/bio/bio_cb.c",
        "deps/openssl/crypto/bio/bio_err.c",
        "deps/openssl/crypto/bio/bio_lib.c",
        "deps/openssl/crypto/bio/bio_meth.c",
        "deps/openssl/crypto/bio/bss_acpt.c",
        "deps/openssl/crypto/bio/bss_bio.c",
        "deps/openssl/crypto/bio/bss_conn.c",
        "deps/openssl/crypto/bio/bss_dgram.c",
        "deps/openssl/crypto/bio/bss_fd.c",
        "deps/openssl/crypto/bio/bss_file.c",
        "deps/openssl/crypto/bio/bss_log.c",
        "deps/openssl/crypto/bio/bss_mem.c",
        "deps/openssl/crypto/bio/bss_null.c",
        "deps/openssl/crypto/bio/bss_sock.c",
        "deps/openssl/crypto/blake2/blake2b.c",
        "deps/openssl/crypto/blake2/blake2s.c",
        "deps/openssl/crypto/blake2/m_blake2b.c",
        "deps/openssl/crypto/blake2/m_blake2s.c",
        "deps/openssl/crypto/bn/bn_add.c",
        "deps/openssl/crypto/bn/bn_blind.c",
        "deps/openssl/crypto/bn/bn_const.c",
        "deps/openssl/crypto/bn/bn_ctx.c",
        "deps/openssl/crypto/bn/bn_depr.c",
        "deps/openssl/crypto/bn/bn_dh.c",
        "deps/openssl/crypto/bn/bn_div.c",
        "deps/openssl/crypto/bn/bn_err.c",
        "deps/openssl/crypto/bn/bn_exp.c",
        "deps/openssl/crypto/bn/bn_exp2.c",
        "deps/openssl/crypto/bn/bn_gcd.c",
        "deps/openssl/crypto/bn/bn_gf2m.c",
        "deps/openssl/crypto/bn/bn_intern.c",
        "deps/openssl/crypto/bn/bn_kron.c",
        "deps/openssl/crypto/bn/bn_lib.c",
        "deps/openssl/crypto/bn/bn_mod.c",
        "deps/openssl/crypto/bn/bn_mont.c",
        "deps/openssl/crypto/bn/bn_mpi.c",
        "deps/openssl/crypto/bn/bn_mul.c",
        "deps/openssl/crypto/bn/bn_nist.c",
        "deps/openssl/crypto/bn/bn_prime.c",
        "deps/openssl/crypto/bn/bn_print.c",
        "deps/openssl/crypto/bn/bn_rand.c",
        "deps/openssl/crypto/bn/bn_recp.c",
        "deps/openssl/crypto/bn/bn_shift.c",
        "deps/openssl/crypto/bn/bn_sqr.c",
        "deps/openssl/crypto/bn/bn_sqrt.c",
        "deps/openssl/crypto/bn/bn_srp.c",
        "deps/openssl/crypto/bn/bn_word.c",
        "deps/openssl/crypto/bn/bn_x931p.c",
        "deps/openssl/crypto/buffer/buf_err.c",
        "deps/openssl/crypto/buffer/buffer.c",
        "deps/openssl/crypto/camellia/cmll_cfb.c",
        "deps/openssl/crypto/camellia/cmll_ctr.c",
        "deps/openssl/crypto/camellia/cmll_ecb.c",
        "deps/openssl/crypto/camellia/cmll_misc.c",
        "deps/openssl/crypto/camellia/cmll_ofb.c",
        "deps/openssl/crypto/cast/c_cfb64.c",
        "deps/openssl/crypto/cast/c_ecb.c",
        "deps/openssl/crypto/cast/c_enc.c",
        "deps/openssl/crypto/cast/c_ofb64.c",
        "deps/openssl/crypto/cast/c_skey.c",
        "deps/openssl/crypto/cmac/cm_ameth.c",
        "deps/openssl/crypto/cmac/cm_pmeth.c",
        "deps/openssl/crypto/cmac/cmac.c",
        "deps/openssl/crypto/cms/cms_asn1.c",
        "deps/openssl/crypto/cms/cms_att.c",
        "deps/openssl/crypto/cms/cms_cd.c",
        "deps/openssl/crypto/cms/cms_dd.c",
        "deps/openssl/crypto/cms/cms_enc.c",
        "deps/openssl/crypto/cms/cms_env.c",
        "deps/openssl/crypto/cms/cms_err.c",
        "deps/openssl/crypto/cms/cms_ess.c",
        "deps/openssl/crypto/cms/cms_io.c",
        "deps/openssl/crypto/cms/cms_kari.c",
        "deps/openssl/crypto/cms/cms_lib.c",
        "deps/openssl/crypto/cms/cms_pwri.c",
        "deps/openssl/crypto/cms/cms_sd.c",
        "deps/openssl/crypto/cms/cms_smime.c",
        "deps/openssl/crypto/comp/c_zlib.c",
        "deps/openssl/crypto/comp/comp_err.c",
        "deps/openssl/crypto/comp/comp_lib.c",
        "deps/openssl/crypto/conf/conf_api.c",
        "deps/openssl/crypto/conf/conf_def.c",
        "deps/openssl/crypto/conf/conf_err.c",
        "deps/openssl/crypto/conf/conf_lib.c",
        "deps/openssl/crypto/conf/conf_mall.c",
        "deps/openssl/crypto/conf/conf_mod.c",
        "deps/openssl/crypto/conf/conf_sap.c",
        "deps/openssl/crypto/conf/conf_ssl.c",
        "deps/openssl/crypto/cpt_err.c",
        "deps/openssl/crypto/cryptlib.c",
        "deps/openssl/crypto/ct/ct_b64.c",
        "deps/openssl/crypto/ct/ct_err.c",
        "deps/openssl/crypto/ct/ct_log.c",
        "deps/openssl/crypto/ct/ct_oct.c",
        "deps/openssl/crypto/ct/ct_policy.c",
        "deps/openssl/crypto/ct/ct_prn.c",
        "deps/openssl/crypto/ct/ct_sct.c",
        "deps/openssl/crypto/ct/ct_sct_ctx.c",
        "deps/openssl/crypto/ct/ct_vfy.c",
        "deps/openssl/crypto/ct/ct_x509v3.c",
        "deps/openssl/crypto/ctype.c",
        "deps/openssl/crypto/cversion.c",
        "deps/openssl/crypto/des/cbc_cksm.c",
        "deps/openssl/crypto/des/cbc_enc.c",
        "deps/openssl/crypto/des/cfb64ede.c",
        "deps/openssl/crypto/des/cfb64enc.c",
        "deps/openssl/crypto/des/cfb_enc.c",
        "deps/openssl/crypto/des/des_enc.c",
        "deps/openssl/crypto/des/ecb3_enc.c",
        "deps/openssl/crypto/des/ecb_enc.c",
        "deps/openssl/crypto/des/fcrypt.c",
        "deps/openssl/crypto/des/fcrypt_b.c",
        "deps/openssl/crypto/des/ofb64ede.c",
        "deps/openssl/crypto/des/ofb64enc.c",
        "deps/openssl/crypto/des/ofb_enc.c",
        "deps/openssl/crypto/des/pcbc_enc.c",
        "deps/openssl/crypto/des/qud_cksm.c",
        "deps/openssl/crypto/des/rand_key.c",
        "deps/openssl/crypto/des/set_key.c",
        "deps/openssl/crypto/des/str2key.c",
        "deps/openssl/crypto/des/xcbc_enc.c",
        "deps/openssl/crypto/dh/dh_ameth.c",
        "deps/openssl/crypto/dh/dh_asn1.c",
        "deps/openssl/crypto/dh/dh_check.c",
        "deps/openssl/crypto/dh/dh_depr.c",
        "deps/openssl/crypto/dh/dh_err.c",
        "deps/openssl/crypto/dh/dh_gen.c",
        "deps/openssl/crypto/dh/dh_kdf.c",
        "deps/openssl/crypto/dh/dh_key.c",
        "deps/openssl/crypto/dh/dh_lib.c",
        "deps/openssl/crypto/dh/dh_meth.c",
        "deps/openssl/crypto/dh/dh_pmeth.c",
        "deps/openssl/crypto/dh/dh_prn.c",
        "deps/openssl/crypto/dh/dh_rfc5114.c",
        "deps/openssl/crypto/dh/dh_rfc7919.c",
        "deps/openssl/crypto/dsa/dsa_ameth.c",
        "deps/openssl/crypto/dsa/dsa_asn1.c",
        "deps/openssl/crypto/dsa/dsa_depr.c",
        "deps/openssl/crypto/dsa/dsa_err.c",
        "deps/openssl/crypto/dsa/dsa_gen.c",
        "deps/openssl/crypto/dsa/dsa_key.c",
        "deps/openssl/crypto/dsa/dsa_lib.c",
        "deps/openssl/crypto/dsa/dsa_meth.c",
        "deps/openssl/crypto/dsa/dsa_ossl.c",
        "deps/openssl/crypto/dsa/dsa_pmeth.c",
        "deps/openssl/crypto/dsa/dsa_prn.c",
        "deps/openssl/crypto/dsa/dsa_sign.c",
        "deps/openssl/crypto/dsa/dsa_vrf.c",
        "deps/openssl/crypto/dso/dso_dl.c",
        "deps/openssl/crypto/dso/dso_dlfcn.c",
        "deps/openssl/crypto/dso/dso_err.c",
        "deps/openssl/crypto/dso/dso_lib.c",
        "deps/openssl/crypto/dso/dso_openssl.c",
        "deps/openssl/crypto/dso/dso_vms.c",
        "deps/openssl/crypto/dso/dso_win32.c",
        "deps/openssl/crypto/ebcdic.c",
        "deps/openssl/crypto/ec/curve25519.c",
        "deps/openssl/crypto/ec/curve448/arch_32/f_impl.c",
        "deps/openssl/crypto/ec/curve448/curve448.c",
        "deps/openssl/crypto/ec/curve448/curve448_tables.c",
        "deps/openssl/crypto/ec/curve448/eddsa.c",
        "deps/openssl/crypto/ec/curve448/f_generic.c",
        "deps/openssl/crypto/ec/curve448/scalar.c",
        "deps/openssl/crypto/ec/ec2_oct.c",
        "deps/openssl/crypto/ec/ec2_smpl.c",
        "deps/openssl/crypto/ec/ec_ameth.c",
        "deps/openssl/crypto/ec/ec_asn1.c",
        "deps/openssl/crypto/ec/ec_check.c",
        "deps/openssl/crypto/ec/ec_curve.c",
        "deps/openssl/crypto/ec/ec_cvt.c",
        "deps/openssl/crypto/ec/ec_err.c",
        "deps/openssl/crypto/ec/ec_key.c",
        "deps/openssl/crypto/ec/ec_kmeth.c",
        "deps/openssl/crypto/ec/ec_lib.c",
        "deps/openssl/crypto/ec/ec_mult.c",
        "deps/openssl/crypto/ec/ec_oct.c",
        "deps/openssl/crypto/ec/ec_pmeth.c",
        "deps/openssl/crypto/ec/ec_print.c",
        "deps/openssl/crypto/ec/ecdh_kdf.c",
        "deps/openssl/crypto/ec/ecdh_ossl.c",
        "deps/openssl/crypto/ec/ecdsa_ossl.c",
        "deps/openssl/crypto/ec/ecdsa_sign.c",
        "deps/openssl/crypto/ec/ecdsa_vrf.c",
        "deps/openssl/crypto/ec/eck_prn.c",
        "deps/openssl/crypto/ec/ecp_mont.c",
        "deps/openssl/crypto/ec/ecp_nist.c",
        "deps/openssl/crypto/ec/ecp_nistp224.c",
        "deps/openssl/crypto/ec/ecp_nistp256.c",
        "deps/openssl/crypto/ec/ecp_nistp521.c",
        "deps/openssl/crypto/ec/ecp_nistputil.c",
        "deps/openssl/crypto/ec/ecp_nistz256.c",
        "deps/openssl/crypto/ec/ecp_oct.c",
        "deps/openssl/crypto/ec/ecp_smpl.c",
        "deps/openssl/crypto/ec/ecx_meth.c",
        "deps/openssl/crypto/engine/eng_all.c",
        "deps/openssl/crypto/engine/eng_cnf.c",
        "deps/openssl/crypto/engine/eng_ctrl.c",
        "deps/openssl/crypto/engine/eng_dyn.c",
        "deps/openssl/crypto/engine/eng_err.c",
        "deps/openssl/crypto/engine/eng_fat.c",
        "deps/openssl/crypto/engine/eng_init.c",
        "deps/openssl/crypto/engine/eng_lib.c",
        "deps/openssl/crypto/engine/eng_list.c",
        "deps/openssl/crypto/engine/eng_openssl.c",
        "deps/openssl/crypto/engine/eng_pkey.c",
        "deps/openssl/crypto/engine/eng_rdrand.c",
        "deps/openssl/crypto/engine/eng_table.c",
        "deps/openssl/crypto/engine/tb_asnmth.c",
        "deps/openssl/crypto/engine/tb_cipher.c",
        "deps/openssl/crypto/engine/tb_dh.c",
        "deps/openssl/crypto/engine/tb_digest.c",
        "deps/openssl/crypto/engine/tb_dsa.c",
        "deps/openssl/crypto/engine/tb_eckey.c",
        "deps/openssl/crypto/engine/tb_pkmeth.c",
        "deps/openssl/crypto/engine/tb_rand.c",
        "deps/openssl/crypto/engine/tb_rsa.c",
        "deps/openssl/crypto/err/err.c",
        "deps/openssl/crypto/err/err_all.c",
        "deps/openssl/crypto/err/err_prn.c",
        "deps/openssl/crypto/evp/bio_b64.c",
        "deps/openssl/crypto/evp/bio_enc.c",
        "deps/openssl/crypto/evp/bio_md.c",
        "deps/openssl/crypto/evp/bio_ok.c",
        "deps/openssl/crypto/evp/c_allc.c",
        "deps/openssl/crypto/evp/c_alld.c",
        "deps/openssl/crypto/evp/cmeth_lib.c",
        "deps/openssl/crypto/evp/digest.c",
        "deps/openssl/crypto/evp/e_aes.c",
        "deps/openssl/crypto/evp/e_aes_cbc_hmac_sha1.c",
        "deps/openssl/crypto/evp/e_aes_cbc_hmac_sha256.c",
        "deps/openssl/crypto/evp/e_aria.c",
        "deps/openssl/crypto/evp/e_bf.c",
        "deps/openssl/crypto/evp/e_camellia.c",
        "deps/openssl/crypto/evp/e_cast.c",
        "deps/openssl/crypto/evp/e_chacha20_poly1305.c",
        "deps/openssl/crypto/evp/e_des.c",
        "deps/openssl/crypto/evp/e_des3.c",
        "deps/openssl/crypto/evp/e_idea.c",
        "deps/openssl/crypto/evp/e_null.c",
        "deps/openssl/crypto/evp/e_old.c",
        "deps/openssl/crypto/evp/e_rc2.c",
        "deps/openssl/crypto/evp/e_rc4.c",
        "deps/openssl/crypto/evp/e_rc4_hmac_md5.c",
        "deps/openssl/crypto/evp/e_rc5.c",
        "deps/openssl/crypto/evp/e_seed.c",
        "deps/openssl/crypto/evp/e_sm4.c",
        "deps/openssl/crypto/evp/e_xcbc_d.c",
        "deps/openssl/crypto/evp/encode.c",
        "deps/openssl/crypto/evp/evp_cnf.c",
        "deps/openssl/crypto/evp/evp_enc.c",
        "deps/openssl/crypto/evp/evp_err.c",
        "deps/openssl/crypto/evp/evp_key.c",
        "deps/openssl/crypto/evp/evp_lib.c",
        "deps/openssl/crypto/evp/evp_pbe.c",
        "deps/openssl/crypto/evp/evp_pkey.c",
        "deps/openssl/crypto/evp/m_md2.c",
        "deps/openssl/crypto/evp/m_md4.c",
        "deps/openssl/crypto/evp/m_md5.c",
        "deps/openssl/crypto/evp/m_md5_sha1.c",
        "deps/openssl/crypto/evp/m_mdc2.c",
        "deps/openssl/crypto/evp/m_null.c",
        "deps/openssl/crypto/evp/m_ripemd.c",
        "deps/openssl/crypto/evp/m_sha1.c",
        "deps/openssl/crypto/evp/m_sha3.c",
        "deps/openssl/crypto/evp/m_sigver.c",
        "deps/openssl/crypto/evp/m_wp.c",
        "deps/openssl/crypto/evp/names.c",
        "deps/openssl/crypto/evp/p5_crpt.c",
        "deps/openssl/crypto/evp/p5_crpt2.c",
        "deps/openssl/crypto/evp/p_dec.c",
        "deps/openssl/crypto/evp/p_enc.c",
        "deps/openssl/crypto/evp/p_lib.c",
        "deps/openssl/crypto/evp/p_open.c",
        "deps/openssl/crypto/evp/p_seal.c",
        "deps/openssl/crypto/evp/p_sign.c",
        "deps/openssl/crypto/evp/p_verify.c",
        "deps/openssl/crypto/evp/pbe_scrypt.c",
        "deps/openssl/crypto/evp/pmeth_fn.c",
        "deps/openssl/crypto/evp/pmeth_gn.c",
        "deps/openssl/crypto/evp/pmeth_lib.c",
        "deps/openssl/crypto/ex_data.c",
        "deps/openssl/crypto/getenv.c",
        "deps/openssl/crypto/hmac/hm_ameth.c",
        "deps/openssl/crypto/hmac/hm_pmeth.c",
        "deps/openssl/crypto/hmac/hmac.c",
        "deps/openssl/crypto/idea/i_cbc.c",
        "deps/openssl/crypto/idea/i_cfb64.c",
        "deps/openssl/crypto/idea/i_ecb.c",
        "deps/openssl/crypto/idea/i_ofb64.c",
        "deps/openssl/crypto/idea/i_skey.c",
        "deps/openssl/crypto/init.c",
        "deps/openssl/crypto/kdf/hkdf.c",
        "deps/openssl/crypto/kdf/kdf_err.c",
        "deps/openssl/crypto/kdf/scrypt.c",
        "deps/openssl/crypto/kdf/tls1_prf.c",
        "deps/openssl/crypto/lhash/lh_stats.c",
        "deps/openssl/crypto/lhash/lhash.c",
        "deps/openssl/crypto/md4/md4_dgst.c",
        "deps/openssl/crypto/md4/md4_one.c",
        "deps/openssl/crypto/md5/md5_dgst.c",
        "deps/openssl/crypto/md5/md5_one.c",
        "deps/openssl/crypto/mdc2/mdc2_one.c",
        "deps/openssl/crypto/mdc2/mdc2dgst.c",
        "deps/openssl/crypto/mem.c",
        "deps/openssl/crypto/mem_dbg.c",
        "deps/openssl/crypto/mem_sec.c",
        "deps/openssl/crypto/modes/cbc128.c",
        "deps/openssl/crypto/modes/ccm128.c",
        "deps/openssl/crypto/modes/cfb128.c",
        "deps/openssl/crypto/modes/ctr128.c",
        "deps/openssl/crypto/modes/cts128.c",
        "deps/openssl/crypto/modes/gcm128.c",
        "deps/openssl/crypto/modes/ocb128.c",
        "deps/openssl/crypto/modes/ofb128.c",
        "deps/openssl/crypto/modes/wrap128.c",
        "deps/openssl/crypto/modes/xts128.c",
        "deps/openssl/crypto/o_dir.c",
        "deps/openssl/crypto/o_fips.c",
        "deps/openssl/crypto/o_fopen.c",
        "deps/openssl/crypto/o_init.c",
        "deps/openssl/crypto/o_str.c",
        "deps/openssl/crypto/o_time.c",
        "deps/openssl/crypto/objects/o_names.c",
        "deps/openssl/crypto/objects/obj_dat.c",
        "deps/openssl/crypto/objects/obj_err.c",
        "deps/openssl/crypto/objects/obj_lib.c",
        "deps/openssl/crypto/objects/obj_xref.c",
        "deps/openssl/crypto/ocsp/ocsp_asn.c",
        "deps/openssl/crypto/ocsp/ocsp_cl.c",
        "deps/openssl/crypto/ocsp/ocsp_err.c",
        "deps/openssl/crypto/ocsp/ocsp_ext.c",
        "deps/openssl/crypto/ocsp/ocsp_ht.c",
        "deps/openssl/crypto/ocsp/ocsp_lib.c",
        "deps/openssl/crypto/ocsp/ocsp_prn.c",
        "deps/openssl/crypto/ocsp/ocsp_srv.c",
        "deps/openssl/crypto/ocsp/ocsp_vfy.c",
        "deps/openssl/crypto/ocsp/v3_ocsp.c",
        "deps/openssl/crypto/pem/pem_all.c",
        "deps/openssl/crypto/pem/pem_err.c",
        "deps/openssl/crypto/pem/pem_info.c",
        "deps/openssl/crypto/pem/pem_lib.c",
        "deps/openssl/crypto/pem/pem_oth.c",
        "deps/openssl/crypto/pem/pem_pk8.c",
        "deps/openssl/crypto/pem/pem_pkey.c",
        "deps/openssl/crypto/pem/pem_sign.c",
        "deps/openssl/crypto/pem/pem_x509.c",
        "deps/openssl/crypto/pem/pem_xaux.c",
        "deps/openssl/crypto/pem/pvkfmt.c",
        "deps/openssl/crypto/pkcs12/p12_add.c",
        "deps/openssl/crypto/pkcs12/p12_asn.c",
        "deps/openssl/crypto/pkcs12/p12_attr.c",
        "deps/openssl/crypto/pkcs12/p12_crpt.c",
        "deps/openssl/crypto/pkcs12/p12_crt.c",
        "deps/openssl/crypto/pkcs12/p12_decr.c",
        "deps/openssl/crypto/pkcs12/p12_init.c",
        "deps/openssl/crypto/pkcs12/p12_key.c",
        "deps/openssl/crypto/pkcs12/p12_kiss.c",
        "deps/openssl/crypto/pkcs12/p12_mutl.c",
        "deps/openssl/crypto/pkcs12/p12_npas.c",
        "deps/openssl/crypto/pkcs12/p12_p8d.c",
        "deps/openssl/crypto/pkcs12/p12_p8e.c",
        "deps/openssl/crypto/pkcs12/p12_sbag.c",
        "deps/openssl/crypto/pkcs12/p12_utl.c",
        "deps/openssl/crypto/pkcs12/pk12err.c",
        "deps/openssl/crypto/pkcs7/bio_pk7.c",
        "deps/openssl/crypto/pkcs7/pk7_asn1.c",
        "deps/openssl/crypto/pkcs7/pk7_attr.c",
        "deps/openssl/crypto/pkcs7/pk7_doit.c",
        "deps/openssl/crypto/pkcs7/pk7_lib.c",
        "deps/openssl/crypto/pkcs7/pk7_mime.c",
        "deps/openssl/crypto/pkcs7/pk7_smime.c",
        "deps/openssl/crypto/pkcs7/pkcs7err.c",
        "deps/openssl/crypto/poly1305/poly1305.c",
        "deps/openssl/crypto/poly1305/poly1305_ameth.c",
        "deps/openssl/crypto/poly1305/poly1305_pmeth.c",
        "deps/openssl/crypto/rand/drbg_ctr.c",
        "deps/openssl/crypto/rand/drbg_lib.c",
        "deps/openssl/crypto/rand/rand_egd.c",
        "deps/openssl/crypto/rand/rand_err.c",
        "deps/openssl/crypto/rand/rand_lib.c",
        "deps/openssl/crypto/rand/rand_unix.c",
        "deps/openssl/crypto/rand/rand_vms.c",
        "deps/openssl/crypto/rand/rand_win.c",
        "deps/openssl/crypto/rand/randfile.c",
        "deps/openssl/crypto/rc2/rc2_cbc.c",
        "deps/openssl/crypto/rc2/rc2_ecb.c",
        "deps/openssl/crypto/rc2/rc2_skey.c",
        "deps/openssl/crypto/rc2/rc2cfb64.c",
        "deps/openssl/crypto/rc2/rc2ofb64.c",
        "deps/openssl/crypto/ripemd/rmd_dgst.c",
        "deps/openssl/crypto/ripemd/rmd_one.c",
        "deps/openssl/crypto/rsa/rsa_ameth.c",
        "deps/openssl/crypto/rsa/rsa_asn1.c",
        "deps/openssl/crypto/rsa/rsa_chk.c",
        "deps/openssl/crypto/rsa/rsa_crpt.c",
        "deps/openssl/crypto/rsa/rsa_depr.c",
        "deps/openssl/crypto/rsa/rsa_err.c",
        "deps/openssl/crypto/rsa/rsa_gen.c",
        "deps/openssl/crypto/rsa/rsa_lib.c",
        "deps/openssl/crypto/rsa/rsa_meth.c",
        "deps/openssl/crypto/rsa/rsa_mp.c",
        "deps/openssl/crypto/rsa/rsa_none.c",
        "deps/openssl/crypto/rsa/rsa_oaep.c",
        "deps/openssl/crypto/rsa/rsa_ossl.c",
        "deps/openssl/crypto/rsa/rsa_pk1.c",
        "deps/openssl/crypto/rsa/rsa_pmeth.c",
        "deps/openssl/crypto/rsa/rsa_prn.c",
        "deps/openssl/crypto/rsa/rsa_pss.c",
        "deps/openssl/crypto/rsa/rsa_saos.c",
        "deps/openssl/crypto/rsa/rsa_sign.c",
        "deps/openssl/crypto/rsa/rsa_ssl.c",
        "deps/openssl/crypto/rsa/rsa_x931.c",
        "deps/openssl/crypto/rsa/rsa_x931g.c",
        "deps/openssl/crypto/seed/seed.c",
        "deps/openssl/crypto/seed/seed_cbc.c",
        "deps/openssl/crypto/seed/seed_cfb.c",
        "deps/openssl/crypto/seed/seed_ecb.c",
        "deps/openssl/crypto/seed/seed_ofb.c",
        "deps/openssl/crypto/sha/sha1_one.c",
        "deps/openssl/crypto/sha/sha1dgst.c",
        "deps/openssl/crypto/sha/sha256.c",
        "deps/openssl/crypto/sha/sha512.c",
        "deps/openssl/crypto/siphash/siphash.c",
        "deps/openssl/crypto/siphash/siphash_ameth.c",
        "deps/openssl/crypto/siphash/siphash_pmeth.c",
        "deps/openssl/crypto/sm2/sm2_crypt.c",
        "deps/openssl/crypto/sm2/sm2_err.c",
        "deps/openssl/crypto/sm2/sm2_pmeth.c",
        "deps/openssl/crypto/sm2/sm2_sign.c",
        "deps/openssl/crypto/sm3/m_sm3.c",
        "deps/openssl/crypto/sm3/sm3.c",
        "deps/openssl/crypto/sm4/sm4.c",
        "deps/openssl/crypto/srp/srp_lib.c",
        "deps/openssl/crypto/srp/srp_vfy.c",
        "deps/openssl/crypto/stack/stack.c",
        "deps/openssl/crypto/store/loader_file.c",
        "deps/openssl/crypto/store/store_err.c",
        "deps/openssl/crypto/store/store_init.c",
        "deps/openssl/crypto/store/store_lib.c",
        "deps/openssl/crypto/store/store_register.c",
        "deps/openssl/crypto/store/store_strings.c",
        "deps/openssl/crypto/threads_none.c",
        "deps/openssl/crypto/threads_pthread.c",
        "deps/openssl/crypto/threads_win.c",
        "deps/openssl/crypto/ts/ts_asn1.c",
        "deps/openssl/crypto/ts/ts_conf.c",
        "deps/openssl/crypto/ts/ts_err.c",
        "deps/openssl/crypto/ts/ts_lib.c",
        "deps/openssl/crypto/ts/ts_req_print.c",
        "deps/openssl/crypto/ts/ts_req_utils.c",
        "deps/openssl/crypto/ts/ts_rsp_print.c",
        "deps/openssl/crypto/ts/ts_rsp_sign.c",
        "deps/openssl/crypto/ts/ts_rsp_utils.c",
        "deps/openssl/crypto/ts/ts_rsp_verify.c",
        "deps/openssl/crypto/ts/ts_verify_ctx.c",
        "deps/openssl/crypto/txt_db/txt_db.c",
        "deps/openssl/crypto/ui/ui_err.c",
        "deps/openssl/crypto/ui/ui_lib.c",
        "deps/openssl/crypto/ui/ui_null.c",
        "deps/openssl/crypto/ui/ui_openssl.c",
        "deps/openssl/crypto/ui/ui_util.c",
        "deps/openssl/crypto/uid.c",
        "deps/openssl/crypto/whrlpool/wp_dgst.c",
        "deps/openssl/crypto/x509/by_dir.c",
        "deps/openssl/crypto/x509/by_file.c",
        "deps/openssl/crypto/x509/t_crl.c",
        "deps/openssl/crypto/x509/t_req.c",
        "deps/openssl/crypto/x509/t_x509.c",
        "deps/openssl/crypto/x509/x509_att.c",
        "deps/openssl/crypto/x509/x509_cmp.c",
        "deps/openssl/crypto/x509/x509_d2.c",
        "deps/openssl/crypto/x509/x509_def.c",
        "deps/openssl/crypto/x509/x509_err.c",
        "deps/openssl/crypto/x509/x509_ext.c",
        "deps/openssl/crypto/x509/x509_lu.c",
        "deps/openssl/crypto/x509/x509_meth.c",
        "deps/openssl/crypto/x509/x509_obj.c",
        "deps/openssl/crypto/x509/x509_r2x.c",
        "deps/openssl/crypto/x509/x509_req.c",
        "deps/openssl/crypto/x509/x509_set.c",
        "deps/openssl/crypto/x509/x509_trs.c",
        "deps/openssl/crypto/x509/x509_txt.c",
        "deps/openssl/crypto/x509/x509_v3.c",
        "deps/openssl/crypto/x509/x509_vfy.c",
        "deps/openssl/crypto/x509/x509_vpm.c",
        "deps/openssl/crypto/x509/x509cset.c",
        "deps/openssl/crypto/x509/x509name.c",
        "deps/openssl/crypto/x509/x509rset.c",
        "deps/openssl/crypto/x509/x509spki.c",
        "deps/openssl/crypto/x509/x509type.c",
        "deps/openssl/crypto/x509/x_all.c",
        "deps/openssl/crypto/x509/x_attrib.c",
        "deps/openssl/crypto/x509/x_crl.c",
        "deps/openssl/crypto/x509/x_exten.c",
        "deps/openssl/crypto/x509/x_name.c",
        "deps/openssl/crypto/x509/x_pubkey.c",
        "deps/openssl/crypto/x509/x_req.c",
        "deps/openssl/crypto/x509/x_x509.c",
        "deps/openssl/crypto/x509/x_x509a.c",
        "deps/openssl/crypto/x509v3/pcy_cache.c",
        "deps/openssl/crypto/x509v3/pcy_data.c",
        "deps/openssl/crypto/x509v3/pcy_lib.c",
        "deps/openssl/crypto/x509v3/pcy_map.c",
        "deps/openssl/crypto/x509v3/pcy_node.c",
        "deps/openssl/crypto/x509v3/pcy_tree.c",
        "deps/openssl/crypto/x509v3/v3_addr.c",
        "deps/openssl/crypto/x509v3/v3_admis.c",
        "deps/openssl/crypto/x509v3/v3_akey.c",
        "deps/openssl/crypto/x509v3/v3_akeya.c",
        "deps/openssl/crypto/x509v3/v3_alt.c",
        "deps/openssl/crypto/x509v3/v3_asid.c",
        "deps/openssl/crypto/x509v3/v3_bcons.c",
        "deps/openssl/crypto/x509v3/v3_bitst.c",
        "deps/openssl/crypto/x509v3/v3_conf.c",
        "deps/openssl/crypto/x509v3/v3_cpols.c",
        "deps/openssl/crypto/x509v3/v3_crld.c",
        "deps/openssl/crypto/x509v3/v3_enum.c",
        "deps/openssl/crypto/x509v3/v3_extku.c",
        "deps/openssl/crypto/x509v3/v3_genn.c",
        "deps/openssl/crypto/x509v3/v3_ia5.c",
        "deps/openssl/crypto/x509v3/v3_info.c",
        "deps/openssl/crypto/x509v3/v3_int.c",
        "deps/openssl/crypto/x509v3/v3_lib.c",
        "deps/openssl/crypto/x509v3/v3_ncons.c",
        "deps/openssl/crypto/x509v3/v3_pci.c",
        "deps/openssl/crypto/x509v3/v3_pcia.c",
        "deps/openssl/crypto/x509v3/v3_pcons.c",
        "deps/openssl/crypto/x509v3/v3_pku.c",
        "deps/openssl/crypto/x509v3/v3_pmaps.c",
        "deps/openssl/crypto/x509v3/v3_prn.c",
        "deps/openssl/crypto/x509v3/v3_purp.c",
        "deps/openssl/crypto/x509v3/v3_skey.c",
        "deps/openssl/crypto/x509v3/v3_sxnet.c",
        "deps/openssl/crypto/x509v3/v3_tlsf.c",
        "deps/openssl/crypto/x509v3/v3_utl.c",
        "deps/openssl/crypto/x509v3/v3err.c",
    });

    const crypto_src_x86_64_extra = [_][]const u8{
        "deps/openssl/crypto/bn/asm/x86_64-gcc.c",
        "deps/openssl/crypto/bn/rsaz_exp.c",
    };

    const crypto_src_aarch64_extra = [_][]const u8{
        "deps/openssl/crypto/armcap.c",
        "deps/openssl/crypto/bn/bn_asm.c",
        "deps/openssl/crypto/camellia/camellia.c",
        "deps/openssl/crypto/camellia/cmll_cbc.c",
        "deps/openssl/crypto/rc4/rc4_enc.c",
        "deps/openssl/crypto/rc4/rc4_skey.c",
        "deps/openssl/crypto/whrlpool/wp_block.c",
    };

    if (target.toTarget().cpu.arch == .x86_64) {
        try crypto_src.appendSlice(&crypto_src_x86_64_extra);
    }

    if (target.toTarget().cpu.arch.isAARCH64()) {
        try crypto_src.appendSlice(&crypto_src_aarch64_extra);
    }

    const asm_x86_64 = [_][]const u8{
        "deps/openssl/crypto/aes/aesni-mb-x86_64.s",
        "deps/openssl/crypto/aes/aesni-sha1-x86_64.s",
        "deps/openssl/crypto/aes/aesni-sha256-x86_64.s",
        "deps/openssl/crypto/aes/aesni-x86_64.s",
        "deps/openssl/crypto/aes/vpaes-x86_64.s",
        "deps/openssl/crypto/bn/rsaz-avx2.s",
        "deps/openssl/crypto/bn/rsaz-x86_64.s",
        "deps/openssl/crypto/bn/x86_64-gf2m.s",
        "deps/openssl/crypto/bn/x86_64-mont.s",
        "deps/openssl/crypto/bn/x86_64-mont5.s",
        "deps/openssl/crypto/camellia/cmll-x86_64.s",
        "deps/openssl/crypto/chacha/chacha-x86_64.s",
        "deps/openssl/crypto/ec/ecp_nistz256-x86_64.s",
        "deps/openssl/crypto/ec/x25519-x86_64.s",
        "deps/openssl/crypto/md5/md5-x86_64.s",
        "deps/openssl/crypto/modes/aesni-gcm-x86_64.s",
        "deps/openssl/crypto/modes/ghash-x86_64.s",
        "deps/openssl/crypto/poly1305/poly1305-x86_64.s",
        "deps/openssl/crypto/rc4/rc4-md5-x86_64.s",
        "deps/openssl/crypto/rc4/rc4-x86_64.s",
        "deps/openssl/crypto/sha/keccak1600-x86_64.s",
        "deps/openssl/crypto/sha/sha1-mb-x86_64.s",
        "deps/openssl/crypto/sha/sha1-x86_64.s",
        "deps/openssl/crypto/sha/sha256-mb-x86_64.s",
        "deps/openssl/crypto/sha/sha256-x86_64.s",
        "deps/openssl/crypto/sha/sha512-x86_64.s",
        "deps/openssl/crypto/whrlpool/wp-x86_64.s",
        "deps/openssl/crypto/x86_64cpuid.s",
    };

    const asm_aarch64 = [_][]const u8{
        "deps/openssl/crypto/aes/aesv8-armx.S",
        "deps/openssl/crypto/aes/vpaes-armv8.S",
        "deps/openssl/crypto/arm64cpuid.S",
        "deps/openssl/crypto/bn/armv8-mont.S",
        "deps/openssl/crypto/chacha/chacha-armv8.S",
        "deps/openssl/crypto/ec/ecp_nistz256-armv8.S",
        "deps/openssl/crypto/modes/ghashv8-armx.S",
        "deps/openssl/crypto/poly1305/poly1305-armv8.S",
        "deps/openssl/crypto/sha/keccak1600-armv8.S",
        "deps/openssl/crypto/sha/sha1-armv8.S",
        "deps/openssl/crypto/sha/sha256-armv8.S",
        "deps/openssl/crypto/sha/sha512-armv8.S",
    };

    if (target.toTarget().cpu.arch == .x86_64) {
        try crypto_src.appendSlice(&asm_x86_64);
    }

    if (target.toTarget().cpu.arch.isAARCH64()) {
        try crypto_src.appendSlice(&asm_aarch64);
    }

    const ssl_src = [_][]const u8{
        "deps/openssl/ssl/bio_ssl.c",
        "deps/openssl/ssl/d1_lib.c",
        "deps/openssl/ssl/d1_msg.c",
        "deps/openssl/ssl/d1_srtp.c",
        "deps/openssl/ssl/methods.c",
        "deps/openssl/ssl/packet.c",
        "deps/openssl/ssl/pqueue.c",
        "deps/openssl/ssl/record/dtls1_bitmap.c",
        "deps/openssl/ssl/record/rec_layer_d1.c",
        "deps/openssl/ssl/record/rec_layer_s3.c",
        "deps/openssl/ssl/record/ssl3_buffer.c",
        "deps/openssl/ssl/record/ssl3_record.c",
        "deps/openssl/ssl/record/ssl3_record_tls13.c",
        "deps/openssl/ssl/s3_cbc.c",
        "deps/openssl/ssl/s3_enc.c",
        "deps/openssl/ssl/s3_lib.c",
        "deps/openssl/ssl/s3_msg.c",
        "deps/openssl/ssl/ssl_asn1.c",
        "deps/openssl/ssl/ssl_cert.c",
        "deps/openssl/ssl/ssl_ciph.c",
        "deps/openssl/ssl/ssl_conf.c",
        "deps/openssl/ssl/ssl_err.c",
        "deps/openssl/ssl/ssl_init.c",
        "deps/openssl/ssl/ssl_lib.c",
        "deps/openssl/ssl/ssl_mcnf.c",
        "deps/openssl/ssl/ssl_quic.c",
        "deps/openssl/ssl/ssl_rsa.c",
        "deps/openssl/ssl/ssl_sess.c",
        "deps/openssl/ssl/ssl_stat.c",
        "deps/openssl/ssl/ssl_txt.c",
        "deps/openssl/ssl/ssl_utst.c",
        "deps/openssl/ssl/statem/extensions.c",
        "deps/openssl/ssl/statem/extensions_clnt.c",
        "deps/openssl/ssl/statem/extensions_cust.c",
        "deps/openssl/ssl/statem/extensions_srvr.c",
        "deps/openssl/ssl/statem/statem.c",
        "deps/openssl/ssl/statem/statem_clnt.c",
        "deps/openssl/ssl/statem/statem_dtls.c",
        "deps/openssl/ssl/statem/statem_lib.c",
        "deps/openssl/ssl/statem/statem_quic.c",
        "deps/openssl/ssl/statem/statem_srvr.c",
        "deps/openssl/ssl/t1_enc.c",
        "deps/openssl/ssl/t1_lib.c",
        "deps/openssl/ssl/t1_trce.c",
        "deps/openssl/ssl/tls13_enc.c",
        "deps/openssl/ssl/tls_srp.c",
    };

    var crypto_cflags = std.ArrayList([]const u8).init(b.allocator);
    try crypto_cflags.appendSlice(&.{
        "-Wall",
        "-DOPENSSLDIR=\"\"",
        "-DENGINESDIR=\"\"",
        "-DL_ENDIAN",
        "-DOPENSSL_PIC",
        "-DOPENSSL_CPUID_OBJ",
        "-DOPENSSL_BN_ASM_MONT",
        "-DSHA1_ASM",
        "-DSHA256_ASM",
        "-DSHA512_ASM",
        "-DKECCAK1600_ASM",
        "-DVPAES_ASM",
        "-DECP_NISTZ256_ASM",
        "-DPOLY1305_ASM",
        "-DNDEBUG",
    });

    if (target.toTarget().cpu.arch == .x86_64) {
        try crypto_cflags.appendSlice(&.{
            "-DOPENSSL_IA32_SSE2",
            "-DOPENSSL_BN_ASM_MONT5",
            "-DOPENSSL_BN_ASM_GF2m",
            "-DRC4_ASM",
            "-DMD5_ASM",
            "-DAESNI_ASM",
            "-DGHASH_ASM",
            "-DX25519_ASM",
        });
    }

    const linux64_cflags_extra = [_][]const u8{
        "-pthread",
        "-Wa,--noexecstack",
        "-Qunused-arguments",
        "-DOPENSSL_USE_NODELETE",
    };

    const macos64_cflags_extra = [_][]const u8{
        "-DOPENSSL_NO_APPLE_CRYPTO_RANDOM",
        "-D_REENTRANT",
    };

    const win64_cflags_extra = [_][]const u8{
        "-Wa,--noexecstack",
        "-Qunused-arguments",
        "-DUNICODE",
        "-D_UNICODE",
        "-DWIN32_LEAN_AND_MEAN",
        "-D_MT",
    };

    if (target.toTarget().cpu.arch == .x86_64 or
        target.toTarget().cpu.arch.isAARCH64())
    {
        if (target.isLinux()) {
            try crypto_cflags.appendSlice(&linux64_cflags_extra);
        }

        if (target.isDarwin()) {
            try crypto_cflags.appendSlice(&macos64_cflags_extra);
        }

        if (target.isWindows()) {
            try crypto_cflags.appendSlice(&win64_cflags_extra);
        }
    }

    const crypto = b.addStaticLibrary("crypto", null);
    crypto.linkLibC();
    crypto.addIncludePath("deps/openssl");
    crypto.addIncludePath("deps/openssl/include");
    crypto.addIncludePath("deps/openssl/crypto");
    crypto.addIncludePath("deps/openssl/crypto/modes");
    crypto.addIncludePath("deps/openssl/crypto/ec/curve448/arch_32");
    crypto.addIncludePath("deps/openssl/crypto/ec/curve448");
    crypto.addCSourceFiles(crypto_src.items, crypto_cflags.items);
    crypto.setTarget(target);
    crypto.setBuildMode(mode);
    crypto.step.dependOn(&config_gen.step);
    crypto.install();

    const ssl = b.addStaticLibrary("ssl", null);
    ssl.linkLibC();
    ssl.addIncludePath("deps/openssl");
    ssl.addIncludePath("deps/openssl/include");
    ssl.addCSourceFiles(&ssl_src, crypto_cflags.items);
    ssl.setTarget(target);
    ssl.setBuildMode(mode);
    ssl.step.dependOn(&config_gen.step);
    ssl.install();

    const ev_src = [_][]const u8{
        "deps/libev/ev.c",
        "deps/libev/event.c",
    };

    var ev_cflags = std.ArrayList([]const u8).init(b.allocator);
    try ev_cflags.appendSlice(&.{
        "-DEV_STANDALONE=1",
        "-DHAVE_CLOCK_GETTIME=1",
        "-DHAVE_DLFCN_H=1",
        "-DHAVE_EVENTFD=1",
        "-DHAVE_FLOOR=1",
        "-DHAVE_INOTIFY_INIT=1",
        "-DHAVE_INTTYPES_H=1",
        "-DHAVE_MEMORY_H=1",
        "-DHAVE_NANOSLEEP=1",
        "-DHAVE_POLL=1",
        "-DHAVE_POLL_H=1",
        "-DHAVE_SELECT=1",
        "-DHAVE_SIGNALFD=1",
        "-DHAVE_STDINT_H=1",
        "-DHAVE_STDLIB_H=1",
        "-DHAVE_STRINGS_H=1",
        "-DHAVE_STRING_H=1",
        "-DHAVE_UNISTD_H=1",
        "-DSTDC_HEADERS=1",
        "-DVERSION=\"4.33\"",
    });

    if (target.isLinux()) {
        try ev_cflags.appendSlice(&.{
            "-DHAVE_SYS_EPOLL_H=1",
            "-DHAVE_SYS_EVENTFD_H=1",
            "-DHAVE_SYS_INOTIFY_H=1",
            "-DHAVE_SYS_SELECT_H=1",
            "-DHAVE_SYS_SIGNALFD_H=1",
            "-DHAVE_SYS_STAT_H=1",
            "-DHAVE_SYS_TIMERFD_H=1",
            "-DHAVE_SYS_TYPES_H=1",

            "-DHAVE_EPOLL_CTL=1",
            "-DHAVE_KERNEL_RWF_T=1",
            "-DHAVE_LINUX_AIO_ABI_H=1",
            "-DHAVE_LINUX_FS_H=1",
        });
    }

    if (target.isDarwin()) {
        try ev_cflags.appendSlice(&.{
            "-DEV_USE_KQUEUE=1",
            "-DHAVE_KQUEUE=1",
        });
    }

    if (target.isWindows()) {
        //try ev_cflags.appendSlice(&.{
        //    "-DEV_USE_IOCP=1",
        //});
    }

    const ev = b.addStaticLibrary("ev", null);
    ev.linkLibC();
    ev.addIncludePath("deps/libev");
    ev.addCSourceFiles(&ev_src, ev_cflags.items);
    ev.setTarget(target);
    ev.setBuildMode(mode);
    ev.install();

    const nghttp3_src = [_][]const u8{
        "deps/nghttp3/lib/nghttp3_rcbuf.c",
        "deps/nghttp3/lib/nghttp3_mem.c",
        "deps/nghttp3/lib/nghttp3_str.c",
        "deps/nghttp3/lib/nghttp3_conv.c",
        "deps/nghttp3/lib/nghttp3_buf.c",
        "deps/nghttp3/lib/nghttp3_ringbuf.c",
        "deps/nghttp3/lib/nghttp3_pq.c",
        "deps/nghttp3/lib/nghttp3_map.c",
        "deps/nghttp3/lib/nghttp3_ksl.c",
        "deps/nghttp3/lib/nghttp3_qpack.c",
        "deps/nghttp3/lib/nghttp3_qpack_huffman.c",
        "deps/nghttp3/lib/nghttp3_qpack_huffman_data.c",
        "deps/nghttp3/lib/nghttp3_err.c",
        "deps/nghttp3/lib/nghttp3_debug.c",
        "deps/nghttp3/lib/nghttp3_conn.c",
        "deps/nghttp3/lib/nghttp3_stream.c",
        "deps/nghttp3/lib/nghttp3_frame.c",
        "deps/nghttp3/lib/nghttp3_tnode.c",
        "deps/nghttp3/lib/nghttp3_vec.c",
        "deps/nghttp3/lib/nghttp3_gaptr.c",
        "deps/nghttp3/lib/nghttp3_idtr.c",
        "deps/nghttp3/lib/nghttp3_range.c",
        "deps/nghttp3/lib/nghttp3_http.c",
        "deps/nghttp3/lib/nghttp3_version.c",
        "deps/nghttp3/lib/nghttp3_balloc.c",
        "deps/nghttp3/lib/nghttp3_opl.c",
        "deps/nghttp3/lib/nghttp3_objalloc.c",
    };

    var nghttp3_cflags = std.ArrayList([]const u8).init(b.allocator);
    try nghttp3_cflags.appendSlice(&.{
        "-DHAVE_CXX17=1",
        "-DHAVE_DECL_BE64TOH=1",
        "-DHAVE_DECL_BSWAP_64=1",
        "-DHAVE_DLFCN_H=1",
        "-DHAVE_INTTYPES_H=1",
        "-DHAVE_MEMMOVE=1",
        "-DHAVE_MEMSET=1",
        "-DHAVE_PTRDIFF_T=1",
        "-DHAVE_STDDEF_H=1",
        "-DHAVE_STDINT_H=1",
        "-DHAVE_STDIO_H=1",
        "-DHAVE_STDLIB_H=1",
        "-DHAVE_STRINGS_H=1",
        "-DHAVE_STRING_H=1",
        "-DHAVE_SYS_STAT_H=1",
        "-DHAVE_SYS_TYPES_H=1",
        "-DHAVE_UNISTD_H=1",
        "-DHAVE_WCHAR_H=1",
        "-DSTDC_HEADERS=1",
        "-DVERSION=\"0.7.1\"",
        "-DBUILDING_NGHTTP3",
        "-fvisibility=hidden",
    });

    if (target.isLinux()) {
        try nghttp3_cflags.appendSlice(&.{
            "-DHAVE_ENDIAN_H=1",
            "-DHAVE_BYTESWAP_H=1",
        });
    }

    if (target.isWindows()) {
        try nghttp3_cflags.appendSlice(&.{
            "-DWIN32_LEAN_AND_MEAN",
        });
    } else {
        try nghttp3_cflags.appendSlice(&.{
            "-DHAVE_ARPA_INET_H=1",
        });
    }

    const nghttp3 = b.addStaticLibrary("nghttp3", null);
    nghttp3.linkLibC();
    nghttp3.addIncludePath("deps/nghttp3/lib");
    nghttp3.addIncludePath("deps/nghttp3/lib/includes");
    nghttp3.addCSourceFiles(&nghttp3_src, nghttp3_cflags.items);
    nghttp3.step.dependOn(&config_gen.step);
    nghttp3.setTarget(target);
    nghttp3.setBuildMode(mode);
    nghttp3.install();

    const ngtcp2_src = [_][]const u8{
        "deps/ngtcp2/lib/ngtcp2_pkt.c",
        "deps/ngtcp2/lib/ngtcp2_conv.c",
        "deps/ngtcp2/lib/ngtcp2_str.c",
        "deps/ngtcp2/lib/ngtcp2_vec.c",
        "deps/ngtcp2/lib/ngtcp2_buf.c",
        "deps/ngtcp2/lib/ngtcp2_conn.c",
        "deps/ngtcp2/lib/ngtcp2_mem.c",
        "deps/ngtcp2/lib/ngtcp2_pq.c",
        "deps/ngtcp2/lib/ngtcp2_map.c",
        "deps/ngtcp2/lib/ngtcp2_rob.c",
        "deps/ngtcp2/lib/ngtcp2_ppe.c",
        "deps/ngtcp2/lib/ngtcp2_crypto.c",
        "deps/ngtcp2/lib/ngtcp2_err.c",
        "deps/ngtcp2/lib/ngtcp2_range.c",
        "deps/ngtcp2/lib/ngtcp2_acktr.c",
        "deps/ngtcp2/lib/ngtcp2_rtb.c",
        "deps/ngtcp2/lib/ngtcp2_strm.c",
        "deps/ngtcp2/lib/ngtcp2_idtr.c",
        "deps/ngtcp2/lib/ngtcp2_gaptr.c",
        "deps/ngtcp2/lib/ngtcp2_ringbuf.c",
        "deps/ngtcp2/lib/ngtcp2_log.c",
        "deps/ngtcp2/lib/ngtcp2_qlog.c",
        "deps/ngtcp2/lib/ngtcp2_cid.c",
        "deps/ngtcp2/lib/ngtcp2_ksl.c",
        "deps/ngtcp2/lib/ngtcp2_cc.c",
        "deps/ngtcp2/lib/ngtcp2_bbr.c",
        "deps/ngtcp2/lib/ngtcp2_bbr2.c",
        "deps/ngtcp2/lib/ngtcp2_addr.c",
        "deps/ngtcp2/lib/ngtcp2_path.c",
        "deps/ngtcp2/lib/ngtcp2_pv.c",
        "deps/ngtcp2/lib/ngtcp2_pmtud.c",
        "deps/ngtcp2/lib/ngtcp2_version.c",
        "deps/ngtcp2/lib/ngtcp2_rst.c",
        "deps/ngtcp2/lib/ngtcp2_window_filter.c",
        "deps/ngtcp2/lib/ngtcp2_opl.c",
        "deps/ngtcp2/lib/ngtcp2_balloc.c",
        "deps/ngtcp2/lib/ngtcp2_objalloc.c",
        "deps/ngtcp2/lib/ngtcp2_unreachable.c",
    };

    const ngtcp2_crypto_openssl_src = [_][]const u8{
        "deps/ngtcp2/crypto/openssl/openssl.c",
        "deps/ngtcp2/crypto/shared.c",
    };

    var ngtcp2_cflags = std.ArrayList([]const u8).init(b.allocator);
    try ngtcp2_cflags.appendSlice(&.{
        "-DENABLE_EXAMPLE_OPENSSL=1",
        "-DHAVE_CXX20=1",
        "-DHAVE_DECL_BE64TOH=1",
        "-DHAVE_DECL_BSWAP_64=1",
        "-DHAVE_DLFCN_H=1",
        "-DHAVE_INTTYPES_H=1",
        "-DHAVE_MEMMOVE=1",
        "-DHAVE_MEMSET=1",
        "-DHAVE_PTRDIFF_T=1",
        "-DHAVE_STDDEF_H=1",
        "-DHAVE_STDINT_H=1",
        "-DHAVE_STDIO_H=1",
        "-DHAVE_STDLIB_H=1",
        "-DHAVE_STRINGS_H=1",
        "-DHAVE_STRING_H=1",
        "-DHAVE_UNISTD_H=1",
        "-DHAVE_WCHAR_H=1",
        "-DSTDC_HEADERS=1",
        "-DVERSION=\"0.11.0\"",

        "-DNGTCP2_STATICLIB",
        "-DBUILDING_NGTCP2",
        "-fvisibility=hidden",
    });

    if (target.isLinux()) {
        try ngtcp2_cflags.appendSlice(&.{
            "-DHAVE_ENDIAN_H=1",
            "-DHAVE_BYTESWAP_H=1",
            "-DHAVE_LINUX_NETLINK_H=1",
            "-DHAVE_LINUX_RTNETLINK_H=1",
            "-DHAVE_ASM_TYPES_H=1",
        });
    }

    if (target.isWindows()) {
        try ngtcp2_cflags.appendSlice(&.{
            "-DWIN32_LEAN_AND_MEAN",
        });
    } else {
        try ngtcp2_cflags.appendSlice(&.{
            "-DHAVE_NETINET_IN_H=1",
            "-DHAVE_ARPA_INET_H=1",
        });
    }

    const ngtcp2 = b.addStaticLibrary("ngtcp2", null);
    ngtcp2.linkLibC();
    ngtcp2.addIncludePath("deps/ngtcp2/lib");
    ngtcp2.addIncludePath("deps/ngtcp2/lib/includes");
    ngtcp2.addCSourceFiles(&ngtcp2_src, ngtcp2_cflags.items);
    ngtcp2.step.dependOn(&config_gen.step);
    ngtcp2.setTarget(target);
    ngtcp2.setBuildMode(mode);
    ngtcp2.install();

    const ngtcp2_crypto_openssl = b.addStaticLibrary("ngtcp2_crypto_openssl", null);
    //ngtcp2_crypto_openssl.linkLibC();
    ngtcp2_crypto_openssl.linkLibrary(ssl);
    ngtcp2_crypto_openssl.linkLibrary(crypto);
    ngtcp2_crypto_openssl.addIncludePath("deps/openssl/include");
    ngtcp2_crypto_openssl.addIncludePath("deps/ngtcp2/lib");
    ngtcp2_crypto_openssl.addIncludePath("deps/ngtcp2/lib/includes");
    ngtcp2_crypto_openssl.addIncludePath("deps/ngtcp2/crypto");
    ngtcp2_crypto_openssl.addIncludePath("deps/ngtcp2/crypto/includes");
    ngtcp2_crypto_openssl.addCSourceFiles(&ngtcp2_crypto_openssl_src, ngtcp2_cflags.items);
    ngtcp2_crypto_openssl.step.dependOn(&config_gen.step);
    ngtcp2_crypto_openssl.setTarget(target);
    ngtcp2_crypto_openssl.setBuildMode(mode);
    ngtcp2_crypto_openssl.install();

    const http_parser_src = [_][]const u8{
        "deps/ngtcp2/third-party/http-parser/http_parser.c",
    };

    const http_parser = b.addStaticLibrary("http_parser", null);
    http_parser.linkLibC();
    http_parser.addIncludePath("deps/ngtcp2/third-party/http_parser");
    http_parser.addCSourceFiles(&http_parser_src, ngtcp2_cflags.items);
    http_parser.step.dependOn(&config_gen.step);
    http_parser.setTarget(target);
    http_parser.setBuildMode(mode);
    http_parser.install();

    var example_cxxflags = std.ArrayList([]const u8).init(b.allocator);
    try example_cxxflags.appendSlice(&.{
        "-DHAVE_CXX20=1",
        "-DHAVE_DECL_BE64TOH=1",
        "-DHAVE_DECL_BSWAP_64=1",
        "-DHAVE_DLFCN_H=1",
        "-DHAVE_INTTYPES_H=1",
        "-DHAVE_MEMMOVE=1",
        "-DHAVE_MEMSET=1",
        "-DHAVE_PTRDIFF_T=1",
        "-DHAVE_STDDEF_H=1",
        "-DHAVE_STDINT_H=1",
        "-DHAVE_STDIO_H=1",
        "-DHAVE_STDLIB_H=1",
        "-DHAVE_STRINGS_H=1",
        "-DHAVE_STRING_H=1",
        "-DHAVE_UNISTD_H=1",
        "-DHAVE_WCHAR_H=1",
        "-DSTDC_HEADERS=1",

        "-DENABLE_EXAMPLE_OPENSSL=1",
        "-DWITH_EXAMPLE_OPENSSL=1",
        "-std=c++20",
    });

    if (target.isLinux()) {
        try ngtcp2_cflags.appendSlice(&.{
            "-DHAVE_ENDIAN_H=1",
            "-DHAVE_BYTESWAP_H=1",
            "-DHAVE_LINUX_NETLINK_H=1",
            "-DHAVE_LINUX_RTNETLINK_H=1",
            "-DHAVE_ASM_TYPES_H=1",
        });
    }

    if (target.isWindows()) {
        try example_cxxflags.appendSlice(&.{
            "-DWIN32_LEAN_AND_MEAN",
        });
    } else {
        try example_cxxflags.appendSlice(&.{
            "-DHAVE_NETINET_IN_H=1",
            "-DHAVE_ARPA_INET_H=1",
        });
    }

    const client_src = [_][]const u8{
        "deps/ngtcp2/examples/client.cc",
        "deps/ngtcp2/examples/client_base.cc",
        "deps/ngtcp2/examples/debug.cc",
        "deps/ngtcp2/examples/util.cc",
        "deps/ngtcp2/examples/shared.cc",
        "deps/ngtcp2/examples/tls_client_context_openssl.cc",
        "deps/ngtcp2/examples/tls_client_session_openssl.cc",
        "deps/ngtcp2/examples/tls_session_base_openssl.cc",
        "deps/ngtcp2/examples/util_openssl.cc",
    };

    const client = b.addExecutable("client", null);
    //client.linkLibC();
    client.linkLibCpp();
    client.linkLibrary(ev);
    client.linkLibrary(ssl);
    client.linkLibrary(crypto);
    client.linkLibrary(ngtcp2);
    client.linkLibrary(nghttp3);
    client.linkLibrary(http_parser);
    client.linkLibrary(ngtcp2_crypto_openssl);
    client.addIncludePath("deps/ngtcp2/examples/");
    client.addIncludePath("deps/nghttp3/lib/includes");
    client.addIncludePath("deps/ngtcp2/third-party");
    client.addIncludePath("deps/ngtcp2/crypto/includes");
    client.addIncludePath("deps/libev");
    client.addIncludePath("deps/openssl/include");
    client.addIncludePath("deps/ngtcp2/lib/includes");
    client.addCSourceFiles(&client_src, example_cxxflags.items);
    client.step.dependOn(&config_gen.step);
    client.setTarget(target);
    client.setBuildMode(mode);
    client.install();

    if (target.isWindows()) {
        client.linkSystemLibrary("ws2_32");
    }

    const server_src = [_][]const u8{
        "deps/ngtcp2/examples/server.cc",
        "deps/ngtcp2/examples/server_base.cc",
        "deps/ngtcp2/examples/debug.cc",
        "deps/ngtcp2/examples/util.cc",
        "deps/ngtcp2/examples/http.cc",
        "deps/ngtcp2/examples/shared.cc",
        "deps/ngtcp2/examples/tls_server_context_openssl.cc",
        "deps/ngtcp2/examples/tls_server_session_openssl.cc",
        "deps/ngtcp2/examples/tls_session_base_openssl.cc",
        "deps/ngtcp2/examples/util_openssl.cc",
    };

    const server = b.addExecutable("server", null);
    //server.linkLibC();
    server.linkLibCpp();
    server.linkLibrary(ev);
    server.linkLibrary(ssl);
    server.linkLibrary(crypto);
    server.linkLibrary(ngtcp2);
    server.linkLibrary(nghttp3);
    server.linkLibrary(http_parser);
    server.linkLibrary(ngtcp2_crypto_openssl);
    server.addIncludePath("deps/ngtcp2/examples/");
    server.addIncludePath("deps/nghttp3/lib/includes");
    server.addIncludePath("deps/ngtcp2/third-party");
    server.addIncludePath("deps/ngtcp2/crypto/includes");
    server.addIncludePath("deps/libev");
    server.addIncludePath("deps/openssl/include");
    server.addIncludePath("deps/ngtcp2/lib/includes");
    server.addCSourceFiles(&server_src, example_cxxflags.items);
    server.step.dependOn(&config_gen.step);
    server.setTarget(target);
    server.setBuildMode(mode);
    server.install();

    if (target.isWindows()) {
        server.linkSystemLibrary("ws2_32");
    }
}
